using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyGradient : MonoBehaviour
{
    public Color bottomColor;
    public Color topColor;
    public Material cloudMaterial;
    public Material skyMaterial;


    private void Update()
    {
        cloudMaterial.SetColor("_BottomColor", bottomColor);
        cloudMaterial.SetColor("_TopColor", topColor);
 
        skyMaterial.SetColor("_BottomColor", bottomColor);
        skyMaterial.SetColor("_TopColor", topColor);
    }
}
